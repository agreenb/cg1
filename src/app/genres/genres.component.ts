import {Component, Input, OnInit} from '@angular/core';
import {Section} from '../sections/section';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit {
  @Input() section: Section;
  genres: string[];
  selectedGenre: string;

  onSelect(genre: string): void {
    this.selectedGenre = genre;
  }

  updateGenres(): void {
    this.genres = this.section.genres;
    this.selectedGenre = this.genres[0];
    console.log("updated");
  }


  constructor() { }

  ngOnInit() {
    this.updateGenres();
  }

}

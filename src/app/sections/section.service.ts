import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Section} from './section';
import {SECTIONS} from './sections';

@Injectable({
  providedIn: 'root'
})
export class SectionService {

  getSections(): Observable<Section[]>{
    return of(SECTIONS);
  }
  constructor() { }
}

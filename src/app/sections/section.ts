export class Section {
  name: string;
  key: string;
  genres: string[];
}

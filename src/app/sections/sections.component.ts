import {Component, Input, OnInit} from '@angular/core';
import {SectionService} from './section.service';
import {Pkg} from '../pkgs/pkg';
import {Section} from './section';
import {Channel} from './channel';
import { CHANNELS } from './channels';


@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})
export class SectionsComponent implements OnInit {
  @Input() pkg: Pkg;

  sections: Section[];
  selectedSection: Section;
  selectedGenre: string;
  channels: Channel[];

  getSections(): void {
    this.sectionService.getSections()
      .subscribe(sections => this.sections = sections);
  }

  onSelect(section: Section): void {
    this.selectedSection = section;
    this.selectedGenre = this.selectedSection.genres[0];
    this.updateChannels();
  }
  updateGenre(genre: string): void {
    this.selectedGenre = genre;
    this.updateChannels();
  }

  updateChannels(): void{
    if(this.selectedGenre === "All"){
      this.channels = CHANNELS;
    } else if (this.selectedGenre === "All Music") {
      this.channels = CHANNELS.filter(genre => genre.progtypetitle === 'Music');
    } else {
      this.channels = CHANNELS.filter(genre => this.selectedGenre === genre.genretitle);
    }
  }

  constructor(private sectionService: SectionService) { }

  ngOnInit() {
    this.getSections();
    this.selectedSection = this.sections[0];
    this.selectedGenre = this.selectedSection.genres[0];
    this.updateChannels();
  }

}


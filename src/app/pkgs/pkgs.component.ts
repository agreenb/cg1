import { Component, OnInit } from '@angular/core';
import {Pkg} from './pkg';
import { PkgService } from './pkg.service';

@Component({
  selector: 'app-pkgs',
  templateUrl: './pkgs.component.html',
  styleUrls: ['./pkgs.component.css']
})
export class PkgsComponent implements OnInit {


  pkgs: Pkg[];
  selectedPkg: Pkg;

  // getPkgs(): void {
  //   this.pkgs = this.pkgService.getPkgs();
  // }

  getPkgs(): void {
    this.pkgService.getPkgs()
      .subscribe(pkgs => this.pkgs = pkgs);
  }

  onSelect(pkg: Pkg): void {
    this.selectedPkg = pkg;
  }

  constructor(private pkgService: PkgService) { }

  ngOnInit() {
    this.getPkgs();
    this.selectedPkg = this.pkgs[0];
  }

}

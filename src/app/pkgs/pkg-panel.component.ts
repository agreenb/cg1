import { Component, OnInit } from '@angular/core';
import {Pkg} from './pkg';
import {PKGS} from './pkgs';


@Component({
  selector: 'app-pkg-panel',
  templateUrl: './pkg-panel.component.html',
  styleUrls: ['./pkgs.component.css']
})
export class PkgPanelComponent implements OnInit {


  panels: Pkg[] = PKGS;

  selectedPkg: Pkg = PKGS[0];

  onSelect(pkg: Pkg): void {
    this.selectedPkg = pkg;
  }

  constructor() { }

  ngOnInit() {
    console.log(PKGS + 'as panels');
  }

}

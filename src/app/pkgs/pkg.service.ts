import { Injectable } from '@angular/core';
import { Pkg } from './pkg';
import { PKGS} from './pkgs';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PkgService {

  getPkgs(): Observable<Pkg[]> {
    return of(PKGS);
  }
  constructor() { }
}

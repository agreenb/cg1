import { TestBed, inject } from '@angular/core/testing';

import { PkgService } from './pkg.service';

describe('PkgService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PkgService]
    });
  });

  it('should be created', inject([PkgService], (service: PkgService) => {
    expect(service).toBeTruthy();
  }));
});

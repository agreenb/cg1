import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PkgsComponent } from './pkgs.component';

describe('PkgsComponent', () => {
  let component: PkgsComponent;
  let fixture: ComponentFixture<PkgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PkgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PkgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PkgsComponent } from './pkgs/pkgs.component';
import {FormsModule} from '@angular/forms';
import {PkgPanelComponent} from './pkgs/pkg-panel.component';
import { SectionsComponent } from './sections/sections.component';
import { GenresComponent } from './genres/genres.component';

@NgModule({
  declarations: [
    AppComponent,
    PkgsComponent,
    PkgPanelComponent,
    SectionsComponent,
    GenresComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
